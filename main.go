package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/template"
	"unicode"

	"github.com/pingcap/tidb/ast"
	"github.com/pingcap/tidb/parser"
	"github.com/zyguan/just"
)

const builtinTpl = `
package {{.Pkg}}

{{- range .Tabs}}
const {{.GoName}}DDL = ` + "`\n{{.GoDDL}}\n`" + `

type {{.GoName}}Cols struct {
	{{- range .Cols}}
	{{.GoName}} string // {{.Verbose}}
	{{- end}}
}

var {{.GoName}} = struct {
	Name string
	Cols {{.GoName}}Cols
}{
	Name: "{{.Name}}",
	Cols: {{.GoName}}Cols {
		{{- range .Cols}}
		{{.GoName}}: "{{.Name}}",
		{{- end}}
	},
}
{{- end}}
`

type Col struct {
	Name string
	Type string
	Opts map[string]string
}

func (c *Col) GoName() string {
	return mapColName(c.Name)
}

func (c *Col) Verbose() string {
	var buf bytes.Buffer
	buf.WriteString(fmt.Sprintf("TYP='%s'", c.Type))
	for _, k := range []string{"DEF", "PK", "AI", "NN", "UQ", "FK"} {
		if v, ok := c.Opts[k]; ok {
			if v == "" {
				buf.WriteString(" " + k)
			} else {
				buf.WriteString(" " + k + "=" + v)
			}
		}
	}
	return buf.String()
}

type TabInfo struct {
	Name string
	Cols []Col
	Refs []*TabInfo
	DDL  string
	coli map[string]int
	refs []string
}

func (t *TabInfo) GoName() string {
	return mapTabName(t.Name)
}

func (t *TabInfo) GoDDL() string {
	return strings.Replace(t.DDL, "`", "`+\"`\"+`", -1)
}

type InfoBuilder struct {
	Info *TabInfo
	coli int
}

func render(tpl *template.Template, fmt bool, pkg string, ts []*TabInfo) []byte {
	var buf bytes.Buffer
	just.TryTo("render template")(nil, tpl.Execute(&buf, struct {
		Pkg  string
		Tabs []*TabInfo
	}{pkg, ts}))

	out := buf.Bytes()
	if fmt {
		out = just.TryTo("format generated code")(format.Source(buf.Bytes())).([]byte)
	}
	return out
}

func camel(s string, capitalize bool) string {
	upper := capitalize
	buf := make([]rune, 0, len(s))
	for _, r := range s {
		if r == '_' {
			upper = true
			continue
		}
		if upper {
			r = unicode.ToUpper(r)
			upper = false
		}
		buf = append(buf, r)
	}
	return string(buf)
}

func mapTabName(s string) string {
	if opts.tabPrefix != "" {
		s = strings.TrimPrefix(s, opts.tabPrefix)
	}
	if opts.tabSuffix != "" {
		s = strings.TrimSuffix(s, opts.tabSuffix)
	}
	return camel(s, true)
}

func mapColName(s string) string {
	return camel(s, true)
}

func makeCol(col *ast.ColumnDef) Col {
	opts := make(map[string]string, len(col.Options))
	for _, o := range col.Options {
		switch o.Tp {
		case ast.ColumnOptionPrimaryKey:
			opts["PK"] = ""
		case ast.ColumnOptionAutoIncrement:
			opts["AI"] = ""
		case ast.ColumnOptionNotNull:
			opts["NN"] = ""
		case ast.ColumnOptionUniqKey:
			opts["UQ"] = ""
		case ast.ColumnOptionDefaultValue:
			var buf bytes.Buffer
			o.Expr.Format(&buf)
			opts["DEF"] = buf.String()
		}
	}
	return Col{
		Name: col.Name.Name.String(),
		Type: strings.ToUpper(col.Tp.String()),
		Opts: opts,
	}
}

func (v *InfoBuilder) Enter(n ast.Node) (ast.Node, bool) {
	switch x := n.(type) {
	case *ast.ColumnDef:
		col := makeCol(x)
		v.Info.Cols = append(v.Info.Cols, col)
		v.Info.coli[col.Name] = v.coli
		v.coli += 1
	case *ast.Constraint:
		opt, ref := "", ""
		switch x.Tp {
		case ast.ConstraintPrimaryKey:
			opt = "PK"
		case ast.ConstraintForeignKey:
			opt = "FK"
			ref = x.Refer.Table.Name.String()
		case ast.ConstraintUniq, ast.ConstraintUniqKey, ast.ConstraintUniqIndex:
			opt = "UQ"
		}
		if opt != "" {
			for _, k := range x.Keys {
				if i, ok := v.Info.coli[k.Column.Name.String()]; ok {
					v.Info.Cols[i].Opts[opt] = ""
				}
			}
		}
		if ref != "" {
			v.Info.refs = append(v.Info.refs, ref)
		}
	case *ast.TableName:
		v.Info.Name = x.Name.String()
	case *ast.CreateTableStmt:
		v.Info = &TabInfo{
			Cols: make([]Col, 0, len(x.Cols)),
			DDL:  strings.TrimSpace(x.Text()),
			coli: make(map[string]int, len(x.Cols)),
			refs: make([]string, 0, 2),
		}
	default:
		return n, true
	}
	return n, false
}

func (v *InfoBuilder) Leave(n ast.Node) (ast.Node, bool) {
	return n, true
}

var opts struct {
	fmt       bool
	pkg       string
	tpl       string
	out       string
	tabPrefix string
	tabSuffix string
}

func main() {
	defer just.HandleAll(func(err error) { log.Fatal(err) })

	flag.BoolVar(&opts.fmt, "fmt", true, "format generated code")
	flag.StringVar(&opts.pkg, "pkg", "main", "package name")
	flag.StringVar(&opts.tpl, "tpl", "", "non-builtin template file")
	flag.StringVar(&opts.out, "o", "", "output file")
	flag.StringVar(&opts.tabPrefix, "pre", "", "table prefix to trim")
	flag.StringVar(&opts.tabSuffix, "suf", "", "table suffix to trim")
	flag.Parse()

	if flag.Arg(0) == "" {
		just.Throw("input sql file argument is missing")
	}

	tplTxt := builtinTpl
	if opts.tpl != "" {
		tplTxt = string(just.TryTo("read template file")(ioutil.ReadFile(opts.tpl)).([]byte))
	}
	tpl := just.TryTo("parse template")(template.New("mytpl").Parse(tplTxt)).(*template.Template)

	raw := just.TryTo("read schema file")(ioutil.ReadFile(flag.Arg(0))).([]byte)
	stmts := just.TryTo("parse schemas")(parser.New().Parse(string(raw), "", "")).([]ast.StmtNode)

	infos, idx := make([]*TabInfo, 0, len(stmts)), make(map[string]*TabInfo, len(stmts))
	for _, stmt := range stmts {
		b := &InfoBuilder{}
		stmt.Accept(b)
		if b.Info != nil {
			infos = append(infos, b.Info)
			idx[b.Info.Name] = b.Info
		}
	}
	for _, info := range infos {
		info.Refs = make([]*TabInfo, len(info.refs))
		for i, n := range info.refs {
			info.Refs[i] = idx[n]
		}
	}

	out := render(tpl, opts.fmt, opts.pkg, infos)
	if opts.out == "" {
		os.Stdout.Write(out)
	} else {
		just.TryTo("write generated file")(nil, ioutil.WriteFile(opts.out, out, 0644))
	}
}
